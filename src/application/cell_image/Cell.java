package application.cell_image;

public class Cell 
{
	private int red;
	private int green;
	private int blue;
	private int grey;
	
	public Cell(int red, int green, int blue)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.grey = 0;
	}
	public Cell(int red, int green, int blue, int grey)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.grey = grey;
	}
	public Cell(int grey)
	{
		this.red = 0;
		this.green = 0;
		this.blue = 0;
		this.grey = grey;
	}
	//Getters
	public int getRed()
	{
		return red;
	}
	public int getBlue()
	{
		return blue;
	}
	public int getGreen()
	{
		return green;
	}
	public int getGrey()
	{
		return grey;
	}
	//Setters
	/*
	public void setRed(int input)
	{
		//Checking bounds
		if(input <= 255)
		{
			if(input >= 0)
			{
				this.red = input;
			}
		}
	}
	public void setGreen(int input)
	{
		//Checking bounds
		if(input <= 255)
		{
			if(input >= 0)
			{
				this.green = input;
			}
		}
	}
	public void setBlue(int input)
	{
		//Checking bounds
		if(input <= 255)
		{
			if(input >= 0)
			{
				this.blue = input;
			}
		}
	}
	public void setGrey(int input)
	{
		//Checking bounds
		if(input <= 255)
		{
			if(input >= 0)
			{
				this.grey = input;
			}
		}
	}
	*/
	public void setRed(int input)
	{
		//Checking bounds
		this.red = input;
	}
	public void setGreen(int input)
	{
		//Checking bounds
		this.green = input;
	}
	public void setBlue(int input)
	{
		//Checking bounds
		this.blue = input;
	}
	public void setGrey(int input)
	{
		this.grey = input;
	}
	//Constructor------------------------------------//
	
		//-----------------------------------------------//
		//Attributes-------------------------------------//
		
		//-----------------------------------------------//
		//Methods----------------------------------------//
		
		//-----------------------------------------------//
}
