package application.cell_image;

import java.util.Optional;

import application.transformations.ImageTransform;

public class CellImage {
	//Constructor------------------------------------//
	public CellImage(Cell[][] cellMap)
	{
		image = cellMap;
		isGreyScaled = false;
	}
	//-----------------------------------------------//
	//Attributes-------------------------------------//
	private Cell[][] image;
	private boolean isGreyScaled;
	//-----------------------------------------------//
	//Methods----------------------------------------//
	public CellImage cloneImage()
	{
		int selfWidth = image.length;
		int selfHeight = image[0].length;
		
		Cell[][] clonedImage = new Cell[selfWidth][selfHeight];
		
		for(int x = 0; x < selfWidth; x++)
		{
			for(int y = 0; y < selfHeight; y++)
			{
				Cell oldCell = image[x][y];
				Cell clonedCell = new Cell(oldCell.getRed(), oldCell.getGreen(), oldCell.getBlue(), oldCell.getGrey());
				clonedImage[x][y] = clonedCell;
			}
		}
		CellImage clonedCellImage = new CellImage(clonedImage);
		if(isGreyScaled)
			clonedCellImage.setIsGrey(true);
		return clonedCellImage;
	}
	public boolean getIsGrey()
	{
		return isGreyScaled;
	}
	public void setIsGrey(boolean input)
	{
		isGreyScaled = input;
	}
	public void applyTransform(ImageTransform mod, CellImage image2)
	{
		if(isGreyScaled)
		{
			Cell[][] newImage = null;
			if(mod.isDyadic())
			{
				newImage = mod.applyMod(this, image2);
			}
			else
			{
				newImage = mod.applyMod(this);
			}
			image = newImage;
		}
		else if(mod.isGreyscaleOp())
		{
			Cell[][] greyVersion = mod.applyMod(this);
			image = greyVersion;
			isGreyScaled = true;
		}
	}
	public int getWidth()
	{
		return image.length;
	}
	public int getHeight()
	{
		return image[0].length;
	}
	public Cell[][] getCellMatrix()
	{
		return image;
	}
	//-----------------------------------------------//
}
