package application.cell_image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public abstract class CellImageFactory 
{
	public static CellImage buildCellImage(String imageURL)
	{
		boolean proceed = true;
		BufferedImage rawImage = null;
		CellImage output = null;
		try
		{
			rawImage = loadBufferedImage(imageURL);
		}
		catch(IOException e)
		{
			proceed = false;
		}
		if(proceed)
		{
			Cell[][] cellMap = extractBufferedImageCells(rawImage);
			output = new CellImage(cellMap);
		}
		return output;
	}
	public static CellImage buildCellImage(BufferedImage rawImage)
	{
		CellImage output = null;
		if(rawImage != null)
		{
			Cell[][] cellMap = extractBufferedImageCells(rawImage);
			output = new CellImage(cellMap);
		}
		return output;
	}
	public static BufferedImage buildBufferedImage(CellImage rawImage)
	{
		Cell[][] cellMatrix = rawImage.getCellMatrix();
		BufferedImage bImage = insertCellsToBufferedImage(cellMatrix);
		return bImage;
	}
	public static BufferedImage buildBufferedImageGrey(CellImage rawImage)
	{
		Cell[][] cellMatrix = rawImage.getCellMatrix();
		BufferedImage bImage = insertCellsToBufferedImageGrey(cellMatrix);
		return bImage;
	}
	public static BufferedImage loadBufferedImage(String imageURL) throws IOException
	{
		File input = new File(imageURL);
		BufferedImage image = ImageIO.read(input);
		return image;
	}
	private static Cell[][] extractBufferedImageCells(BufferedImage image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
       
		Cell[][] cellMatrix = new Cell[width][height];
       
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
				Color c = new Color(image.getRGB(i, j));
				cellMatrix[i][j] = new Cell(c.getRed(), c.getGreen(), c.getBlue());
			}
		}
		return cellMatrix;
	}
	private static BufferedImage insertCellsToBufferedImage(Cell[][] cellMatrix)
	{
		int width = cellMatrix.length;
		int height = cellMatrix[0].length;
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				Cell target = cellMatrix[x][y];
				Color in = new Color(target.getRed(), target.getGreen(), target.getBlue());
				int rgbColor = in.getRGB();
				newImage.setRGB(x, y, rgbColor);
			}
		}
		return newImage;	
	}
	private static BufferedImage insertCellsToBufferedImageGrey(Cell[][] cellMatrix)
	{
		int width = cellMatrix.length;
		int height = cellMatrix[0].length;
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				Cell target = cellMatrix[x][y];
				int greyColor = target.getGrey();
				Color in = new Color(greyColor, greyColor, greyColor);
				int rgbColor = in.getRGB();
				newImage.setRGB(x, y, rgbColor);
			}
		}
		return newImage;	
	}
}
