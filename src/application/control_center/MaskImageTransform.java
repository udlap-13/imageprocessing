package application.control_center;

import java.util.ArrayList;
import java.util.List;

import application.transformations.ImageTransform;
import application.transformations.implementation.mask.AverageFilter;
import application.transformations.implementation.mask.LaplacianMaskVersion1;
import application.transformations.implementation.mask.LaplacianMaskVersion2;
import application.transformations.implementation.mask.PrewittOperator01;
import application.transformations.implementation.mask.PrewittOperator02;
import application.transformations.implementation.mask.PrewittOperator03;
import application.transformations.implementation.mask.PrewittOperator04;
import application.transformations.implementation.mask.PrewittOperatorD01;
import application.transformations.implementation.mask.PrewittOperatorD02;
import application.transformations.implementation.mask.PrewittOperatorD03;
import application.transformations.implementation.mask.PrewittOperatorD04;
import application.transformations.implementation.mask.SobelOperator01;
import application.transformations.implementation.mask.SobelOperator02;
import application.transformations.implementation.mask.compass.PrewittCompass;
import application.transformations.implementation.mask.dual.PrewittOperator;
import application.transformations.implementation.mask.dual.SobelOperator;
import application.transformations.implementation.unique.MedianFilter;

public abstract class MaskImageTransform 
{
	//Applied transform list
	public static List<ImageTransform> maskTransformList = new ArrayList<ImageTransform>();
	
	//Average Filter
	public static AverageFilter averageFilter = new AverageFilter();
	//Sobel
	public static SobelOperator01 sobelOperator01 = new SobelOperator01();
	public static SobelOperator02 sobelOperator02 = new SobelOperator02();
	public static SobelOperator sobelOperator = new SobelOperator();
	//Prewitt
	public static PrewittOperator01 prewittOperator01 = new PrewittOperator01();
	public static PrewittOperator02 prewittOperator02 = new PrewittOperator02();
	//Extra prewitt for compass
	public static PrewittOperator03 prewittOperator03 = new PrewittOperator03();
	public static PrewittOperator04 prewittOperator04 = new PrewittOperator04();
	
	public static PrewittOperatorD01 prewittOperatorD01 = new PrewittOperatorD01();
	public static PrewittOperatorD02 prewittOperatorD02 = new PrewittOperatorD02();
	
	public static PrewittOperatorD03 prewittOperatorD03 = new PrewittOperatorD03();
	public static PrewittOperatorD04 prewittOperatorD04 = new PrewittOperatorD04();
	//--//
	public static PrewittOperator prewittOperator = new PrewittOperator();
	public static PrewittCompass prewittCompass = new PrewittCompass();
	//Laplacian
	public static LaplacianMaskVersion1 laplacianVersion1 = new LaplacianMaskVersion1();
	public static LaplacianMaskVersion2 laplacianVersion2 = new LaplacianMaskVersion2();
	//Median
	public static MedianFilter medianFilter = new MedianFilter();
}
