package application.control_center;

import application.transformations.ImageTransform;
import application.transformations.implementation.dyadic.ImageAddition;
import application.transformations.implementation.dyadic.ImageMultiplication;
import application.transformations.implementation.dyadic.ImageSubtraction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thagus on 08/03/2016.
 */
public abstract class DyadicImageTransform {
    //Applied transform list
    public static List<ImageTransform> transformList = new ArrayList<ImageTransform>();

    //Addition
    public static ImageAddition imageAddition = new ImageAddition();
    //Subtraction
    public static ImageSubtraction imageSubtraction = new ImageSubtraction();
    //Multiplication
    public static ImageMultiplication imageMultiplication = new ImageMultiplication();
}
