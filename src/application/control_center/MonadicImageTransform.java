package application.control_center;

import java.util.ArrayList;
import java.util.List;

import application.transformations.ImageTransform;
import application.transformations.implementation.AverageGreyscale;
import application.transformations.implementation.BinThreshold;
import application.transformations.implementation.GreyLevelReduction;
import application.transformations.implementation.GreyscaleThreshold;
import application.transformations.implementation.InvBinThreshold;
import application.transformations.implementation.InvGreyscaleThreshold;
import application.transformations.implementation.InvThreshold;
import application.transformations.implementation.LightnessGreyscale;
import application.transformations.implementation.LuminosityGreyscale;
import application.transformations.implementation.Stretch;
import application.transformations.implementation.Threshold;
import application.transformations.implementation.inverseOperator;
import application.transformations.implementation.unique.ImageRotation;
import application.transformations.implementation.unique.ImageZoom;

public abstract class MonadicImageTransform 
{
	//Applied transform list
	public static List<ImageTransform> transformList = new ArrayList<ImageTransform>();

	//Grayscale operations-------------
	//Luminosity
	public static LuminosityGreyscale luminosityGreyscale = new LuminosityGreyscale();
	//Average 
	public static AverageGreyscale averageGreyscale = new AverageGreyscale();
	//Lightness
	public static LightnessGreyscale lightnessGreyscale = new LightnessGreyscale();
	//Monadic operations---------------
	//Inverse Operator
	public static inverseOperator inverse = new inverseOperator();

	public static Threshold threshold = new Threshold();

	public static InvThreshold invThreshold = new InvThreshold();

	public static BinThreshold binThreshold = new BinThreshold();

	public static InvBinThreshold invBinThreshold = new InvBinThreshold();

	public static GreyscaleThreshold greyscaleThreshold = new GreyscaleThreshold();

	public static InvGreyscaleThreshold invGreyscaleThreshold = new InvGreyscaleThreshold();

	public static Stretch stretch = new Stretch();

	public static GreyLevelReduction greyLevelReduction = new GreyLevelReduction();
	
	//Unique image alterations
	public static ImageRotation imageRotation = new ImageRotation();
	
	public static ImageZoom imageZoom = new ImageZoom();
}
