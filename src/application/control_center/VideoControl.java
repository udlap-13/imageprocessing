package application.control_center;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import application.cell_image.CellImage;
import application.cell_image.CellImageFactory;
import application.transformations.ImageTransform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class VideoControl 
{
	@FXML
	private ImageView color_view;
	//Constructor------------------------------------//
	public VideoControl()
	{
		init();
	}
	public VideoControl(ImageView i1, ImageView i2)
	{
		colorCanvas = i1;
		greyCanvas = i2;
		init();
	}
	//-----------------------------------------------//
	//Attributes-------------------------------------//
	private ImageView colorCanvas;
	private ImageView greyCanvas;
	private ScheduledExecutorService executor;
	private VideoCapture hardwareLink;
	private boolean camActive;
	private int targetFPS = 60;
	//For static image processing--------------------//
	private Image img1;
	private Image img2;
	private CellImage cellImg2;
	//-----------------------------------------------//
	
	//-----------------------------------------------//
	//Methods----------------------------------------//
	private void init()
	{
		hardwareLink = new VideoCapture();
		camActive = false;
	}
	public void toggleCam() throws Exception
	{
		if(camActive)
		{ shutdownCam(); }
		else
		{ activateCam(); }
	}
	public void setColorCanvas(ImageView i1)
	{
		this.colorCanvas = i1;
	}
	public void setGreyCanvas(ImageView i2)
	{
		this.greyCanvas = i2;
	}
	private void activateCam() throws Exception
	{
		//Activating hardware link
		hardwareLink.open(0);
		//Safety check, are we allowed in the camera at all.
		if(hardwareLink.isOpened())
		{
			//We know the camera is active and working well now
			camActive = true;
			//Creating a thread that will capture and attach the images to our canvas
			Runnable captureThread = new Runnable()
					{
						@Override
						public void run() {
							
							//Capturing current frame from camera.
							Image snapshot = captureFrame();
							//Attaching our frame to our color canvas.
							colorCanvas.setImage(snapshot);
							//Now we will fill our greyscale version
							//TESTING//
							Image greySnapshot = captureFrame();
							//--//
							BufferedImage moddableImage = fxImage2BufferedImage(greySnapshot);
							CellImage moddableCellImage = CellImageFactory.buildCellImage(moddableImage);
							List<ImageTransform> transformList = MonadicImageTransform.transformList;
							for(int i = 0; i < transformList.size(); i++)
							{
								if(!transformList.get(i).isDyadic())
								{
									moddableCellImage.applyTransform(transformList.get(i), null);
								}
								else if(cellImg2 != null)
								{
									moddableCellImage.applyTransform(transformList.get(i), cellImg2);
								}
							}
							moddableImage = CellImageFactory.buildBufferedImageGrey(moddableCellImage);
							greySnapshot = bufferedImage2FxImage(moddableImage);
							//--//
							greyCanvas.setImage(greySnapshot);
						}
					};
			//Setting up our thread with our new timed executor
			executor = Executors.newSingleThreadScheduledExecutor();
			//Calculating our desired millisecond refresh based on our desired FPS
			int millisecRefresh = 1000 / targetFPS;
			//We configure the thread and launch it.
			executor.scheduleAtFixedRate(captureThread, 0, millisecRefresh, TimeUnit.MILLISECONDS);
		}
		else
		{
			throw new Exception("Not allowed to use camera");
		}
	}
	public void loadImageOne(String imgURL)
	{
		BufferedImage rawBImg = loadImageFromURL(imgURL);
		img1 = bufferedImage2FxImage(rawBImg);
		System.out.println(img1);
		colorCanvas.setImage(img1);
		//Now we will fill our greyscale version
		//TESTING//
		Image greySnapshot = bufferedImage2FxImage(loadImageFromURL(imgURL));
		//--//
		BufferedImage moddableImage = fxImage2BufferedImage(greySnapshot);
		CellImage moddableCellImage = CellImageFactory.buildCellImage(moddableImage);
		List<ImageTransform> transformList = MonadicImageTransform.transformList;
		for(int i = 0; i < transformList.size(); i++)
		{
			if(!transformList.get(i).isDyadic())
			{
				moddableCellImage.applyTransform(transformList.get(i), null);
			}
			else if(cellImg2 != null)
			{
				moddableCellImage.applyTransform(transformList.get(i), cellImg2);
			}
		}
		moddableImage = CellImageFactory.buildBufferedImageGrey(moddableCellImage);
		greySnapshot = bufferedImage2FxImage(moddableImage);
		//--//
		greyCanvas.setImage(greySnapshot);
	}
	public void loadImageTwo(String imgURL)
	{
		BufferedImage rawBImg = loadImageFromURL(imgURL);
		img2 = bufferedImage2FxImage(rawBImg);
		cellImg2 = CellImageFactory.buildCellImage(rawBImg);
		cellImg2.applyTransform(MonadicImageTransform.luminosityGreyscale, null);
	}
	public void reloadMods()
	{
		if(this.img1!= null)
		{
			System.out.println("Reload Mods");
			BufferedImage moddableImage = fxImage2BufferedImage(img1);
			CellImage moddableCellImage = CellImageFactory.buildCellImage(moddableImage);
			List<ImageTransform> transformList = MonadicImageTransform.transformList;
			for(int i = 0; i < transformList.size(); i++)
			{
				if(!transformList.get(i).isDyadic())
				{
					moddableCellImage.applyTransform(transformList.get(i), null);
				}
				else if(cellImg2 != null)
				{
					moddableCellImage.applyTransform(transformList.get(i), cellImg2);
				}
			}
			moddableImage = CellImageFactory.buildBufferedImageGrey(moddableCellImage);
			Image greySnapshot = bufferedImage2FxImage(moddableImage);
			//--//
			greyCanvas.setImage(greySnapshot);
			
		}
	}
	//---//
	private void shutdownCam()
	{
		//We are no longer using our camera, updating system awareness
		camActive = false;
		//Trying to stop the camera
		try{
			//Instructing our thread to stop
			executor.shutdown();
			//Calculating our desired millisecond refresh based on our desired FPS
			int millisecRefresh = 1000 / targetFPS;
			//Preparing a timeout in case it fails
			executor.awaitTermination(millisecRefresh, TimeUnit.MILLISECONDS);
		}
		catch(InterruptedException ex)
		{
			System.err.println("Unable to terminate thread, releasing hardware only: " + ex);
		}
		//Releasing hardware camera connection
		hardwareLink.release();
		//Emptying frame
		colorCanvas.setImage(null);
	}
	private Image captureFrame()
	{
		//Initiating local variables
		Image extracted = null;
		Mat rawInput = new Mat();
		//Checking to see if we are recording
		if(hardwareLink.isOpened())
		{
			try
			{
				//Using openCV to get raw input from our camera.
				hardwareLink.read(rawInput);
				//Safety check to see if the frame has something
				if(!rawInput.empty())
				{
					//Converting our raw mat input to a general image.
					extracted = cvMat2Img(rawInput);
				}
			}
			catch (Exception e)
			{
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		}
		return extracted;
	}
	private Image captureGreyFrame()
	{
		//Initiating local variables
		Image extracted = null;
		Mat rawInput = new Mat();
		//Checking to see if we are recording
		if(hardwareLink.isOpened())
		{
			try
			{
				//Using openCV to get raw input from our camera.
				hardwareLink.read(rawInput);
				//Safety check to see if the frame has something
				if(!rawInput.empty())
				{
					//Processing the image, here we can add a grey scale if we want.
					Imgproc.cvtColor(rawInput, rawInput, Imgproc.COLOR_BGR2GRAY);
					//Converting our raw mat input to a general image.
					extracted = cvMat2Img(rawInput);
				}
			}
			catch (Exception e)
			{
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		}
		return extracted;
	}
	private Image cvMat2Img(Mat rawInput)
	{
		// create a temporary buffer
		MatOfByte buffer = new MatOfByte();
		// encode the frame in the buffer
		Imgcodecs.imencode(".png", rawInput, buffer);
		// build and return an Image created from the image encoded in the
		// buffer
		return new Image(new ByteArrayInputStream(buffer.toArray()));
	}
	private BufferedImage fxImage2BufferedImage(Image fxImage)
	{
		BufferedImage bImage = SwingFXUtils.fromFXImage(fxImage, null);
		return bImage;
	}
	private Image bufferedImage2FxImage(BufferedImage bImage)
	{
		Image fxImage = SwingFXUtils.toFXImage(bImage, null);
		return fxImage;
	}
	private BufferedImage loadImageFromURL(String imgURL)
	{
		//Loading the first image
		//Attributes needed for runtime
		boolean proceed = true;
		BufferedImage loadedImage = null;
		//Trying to load a buffered image from the URL.
		try{
			loadedImage = CellImageFactory.loadBufferedImage(imgURL);
		}
		//If something blows up, then do not proceed with the program
		catch(Exception e){
			proceed = false;
			e.printStackTrace();
			}
		return loadedImage;
	}
	//-----------------------------------------------//
}
