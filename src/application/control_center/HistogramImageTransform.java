package application.control_center;

import application.transformations.ImageTransform;
import application.transformations.implementation.histogram.HistogramEqualization;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thagus on 08/03/2016.
 */
public abstract class HistogramImageTransform {
    //Applied transform list
    public static List<ImageTransform> transformList = new ArrayList<ImageTransform>();

    //Equalization
    public static HistogramEqualization histogramEqualization = new HistogramEqualization();
}
