package application;

import application.control_center.DyadicImageTransform;
import application.control_center.HistogramImageTransform;
import application.control_center.MaskImageTransform;
import application.control_center.MonadicImageTransform;
import application.control_center.VideoControl;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class Controller 
{
	//Outside Objects-------------------------------------------//
	private VideoControl videoControl;
	//Boolean flags---------------------------------------------//
	private boolean flag_inverse = false;
	private boolean flag_threshold = false;
	private boolean flag_inv_threshold = false;
	private boolean flag_bin_threshold = false;
	private boolean flag_inv_bin_threshold = false;
	private boolean flag_greyscale_threshold = false;
	private boolean flag_inv_greyscale_threshold = false;
	private boolean flag_stretch = false;
	private boolean flag_grey_level_reduction = false;
	
	private boolean flag_image_addition = false;
	private boolean flag_image_subtraction = false;
	private boolean flag_image_multiplication = false;
	private int flag_image_rotation = 0;
	private boolean flag_image_zoom = false;
	
	private boolean flag_average_filter = false;
	private boolean flag_median_filter = false;
	private boolean flag_sobel = false;
	private boolean flag_prewitt = false;
	private boolean flag_compass = false;
	private boolean flag_laplacian_one = false;
	private boolean flag_laplacian_two = false;
	
	private boolean flag_luminosity = false;
	private boolean flag_average = false;
	private boolean flag_lightness = false;
	
	private boolean flag_histogram_equalization = false;
	
	private boolean flag_cam_active = false;
	private boolean flag_first_image_loaded = false;
	private boolean flag_second_image_loaded = false;
	//----------------------------------------------------------//
	//Getting most relevant components from view:
	//Buttons------------------------------------------------------//
	@FXML
	private Button cam_inverse;
	@FXML
	private Button cam_threshold;
	@FXML
	private Button cam_inv_threshold;
	@FXML
	private Button cam_bin_threshold;
	@FXML
	private Button cam_inv_bin_threshold;
	@FXML
	private Button cam_greyscale_threshold;
	@FXML
	private Button cam_inv_greyscale_threshold;
	@FXML
	private Button cam_stretch;
	@FXML
	private Button cam_grey_level_reduction;
	@FXML
	private Button cam_image_addition;
	@FXML
	private Button cam_image_subtraction;
	@FXML
	private Button cam_image_multiplication;
	@FXML
	private Button cam_image_rotation;
	@FXML
	private Button cam_image_zoom;
	@FXML
	private Button cam_button;
	@FXML
	private Button btn_luminosity;
	@FXML
	private Button btn_average;
	@FXML
	private Button btn_lightness;
	@FXML
	private Button cam_average_filter;
	@FXML
	private Button cam_median_filter;
	@FXML
	private Button cam_sobel;
	@FXML
	private Button cam_prewitt;
	@FXML
	private Button cam_compass;
	@FXML
	private Button cam_laplacian_one;
	@FXML
	private Button cam_laplacian_two;
	@FXML
	private Button btn_image_1;
	@FXML
	private Button btn_image_2;
	@FXML
	private Button cam_histogram_equalization;
	//-----------------------------------------------------------//
	//Containers-------------------------------------------------//
	@FXML
	private ImageView color_view;
	@FXML
	private ImageView grey_view;
	//----------------------------------------------------------//
	//Text Stuff------------------------------------------------//
	@FXML
	private TextField ta_image_url;
	//----------------------------------------------------------//
	//Now adding the listeners to the application---------------//
	@FXML
	protected void inverse(ActionEvent event)
	{
		if(!flag_inverse){
			MonadicImageTransform.transformList.add(MonadicImageTransform.inverse);
			videoControl.reloadMods();
			flag_inverse = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.inverse);
			videoControl.reloadMods();
			flag_inverse = false;
		}
	}
	@FXML
	protected void threshold(ActionEvent event)
	{
		if(!flag_threshold){
			MonadicImageTransform.transformList.add(MonadicImageTransform.threshold);
			videoControl.reloadMods();
			flag_threshold = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.threshold);
			videoControl.reloadMods();
			flag_threshold = false;
		}
	}
	@FXML
	protected void invThreshold(ActionEvent event)
	{
		if(!flag_inv_threshold){
			MonadicImageTransform.transformList.add(MonadicImageTransform.invThreshold);
			videoControl.reloadMods();
			flag_inv_threshold = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.invThreshold);
			videoControl.reloadMods();
			flag_inv_threshold = false;
		}
	}
	@FXML
	protected void binThreshold(ActionEvent event)
	{
		if(!flag_bin_threshold){
			MonadicImageTransform.transformList.add(MonadicImageTransform.binThreshold);
			videoControl.reloadMods();
			flag_bin_threshold = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.binThreshold);
			videoControl.reloadMods();
			flag_bin_threshold = false;
		}
	}
	@FXML
	protected void invBinThreshold(ActionEvent event)
	{
		if(!flag_inv_bin_threshold){
			MonadicImageTransform.transformList.add(MonadicImageTransform.invBinThreshold);
			videoControl.reloadMods();
			flag_inv_bin_threshold = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.invBinThreshold);
			videoControl.reloadMods();
			flag_inv_bin_threshold = false;
		}
	}
	@FXML
	protected void greyscaleThreshold(ActionEvent event)
	{
		if(!flag_greyscale_threshold){
			MonadicImageTransform.transformList.add(MonadicImageTransform.greyscaleThreshold);
			videoControl.reloadMods();
			flag_greyscale_threshold = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.greyscaleThreshold);
			videoControl.reloadMods();
			flag_greyscale_threshold = false;
		}
	}
	@FXML
	protected void invGreyscaleThreshold(ActionEvent event)
	{
		if(!flag_inv_greyscale_threshold){
			MonadicImageTransform.transformList.add(MonadicImageTransform.invGreyscaleThreshold);
			videoControl.reloadMods();
			flag_inv_greyscale_threshold = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.invGreyscaleThreshold);
			videoControl.reloadMods();
			flag_inv_greyscale_threshold = false;
		}
	}
	@FXML
	protected void stretch(ActionEvent event)
	{
		if(!flag_stretch){
			MonadicImageTransform.transformList.add(MonadicImageTransform.stretch);
			videoControl.reloadMods();
			flag_stretch = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.stretch);
			videoControl.reloadMods();
			flag_stretch = false;
		}
	}
	@FXML
	protected void greyLevelReduction(ActionEvent event)
	{
		if(!flag_grey_level_reduction){
			MonadicImageTransform.transformList.add(MonadicImageTransform.greyLevelReduction);
			videoControl.reloadMods();
			flag_grey_level_reduction = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.greyLevelReduction);
			videoControl.reloadMods();
			flag_grey_level_reduction = false;
		}
	}
	@FXML
	protected void imageAddition(ActionEvent event)
	{
		if(!flag_image_addition && flag_second_image_loaded){
			MonadicImageTransform.transformList.add(DyadicImageTransform.imageAddition);
			videoControl.reloadMods();
			flag_image_addition = true;
		} else{
			MonadicImageTransform.transformList.remove(DyadicImageTransform.imageAddition);
			videoControl.reloadMods();
			flag_image_addition = false;
		}
	}
	@FXML
	protected void imageSubtraction(ActionEvent event)
	{
		if(!flag_image_subtraction && flag_second_image_loaded){
			MonadicImageTransform.transformList.add(DyadicImageTransform.imageSubtraction);
			videoControl.reloadMods();
			flag_image_subtraction = true;
		} else{
			MonadicImageTransform.transformList.remove(DyadicImageTransform.imageSubtraction);
			videoControl.reloadMods();
			flag_image_subtraction = false;
		}
	}
	@FXML
	protected void imageMultiplication(ActionEvent event)
	{
		if(!flag_image_multiplication && flag_second_image_loaded){
			MonadicImageTransform.transformList.add(DyadicImageTransform.imageMultiplication);
			videoControl.reloadMods();
			flag_image_multiplication = true;
		} else{
			MonadicImageTransform.transformList.remove(DyadicImageTransform.imageMultiplication);
			videoControl.reloadMods();
			flag_image_multiplication = false;
		}
	}
	@FXML
	protected void imageRotation(ActionEvent event)
	{
		boolean proceed = true;
		switch (flag_image_rotation)
		{
		case 0:
			//To 45�
			MonadicImageTransform.imageRotation.setSinTheta(0.7071);
			MonadicImageTransform.imageRotation.setCosTheta(0.7071);
			flag_image_rotation++;
			break;
		case 1:
			//To 90�
			MonadicImageTransform.imageRotation.setSinTheta(1);
			MonadicImageTransform.imageRotation.setCosTheta(0);
			flag_image_rotation++;
			break;
		case 2:
			//To 135�
			MonadicImageTransform.imageRotation.setSinTheta(0.7071);
			MonadicImageTransform.imageRotation.setCosTheta(-0.7071);
			flag_image_rotation++;
			break;
		case 3:
			//To 180�
			MonadicImageTransform.imageRotation.setSinTheta(0);
			MonadicImageTransform.imageRotation.setCosTheta(-1);
			flag_image_rotation++;
			break;
		case 4:
			//To 225�
			MonadicImageTransform.imageRotation.setSinTheta(-0.7071);
			MonadicImageTransform.imageRotation.setCosTheta(-0.7071);
			flag_image_rotation++;
			break;
		case 5:
			//To 270�
			MonadicImageTransform.imageRotation.setSinTheta(-1);
			MonadicImageTransform.imageRotation.setCosTheta(0);
			flag_image_rotation++;
			break;
		case 6:
			//To 315�
			MonadicImageTransform.imageRotation.setSinTheta(-0.7071);
			MonadicImageTransform.imageRotation.setCosTheta(0.7071);
			flag_image_rotation++;
			break;
		case 7:
			proceed = false;
			flag_image_rotation = 0;
			break;
		default:
			proceed = false;
			break;
		}
		if(proceed)
		{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.imageRotation);
			MonadicImageTransform.transformList.add(MonadicImageTransform.imageRotation);
			videoControl.reloadMods();
		}
		else
		{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.imageRotation);
			videoControl.reloadMods();
		}
	}
	@FXML
	protected void imageZoom(ActionEvent event)
	{
		if(!flag_image_zoom){
			MonadicImageTransform.transformList.add(MonadicImageTransform.imageZoom);
			videoControl.reloadMods();
			flag_image_zoom = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.imageZoom);
			videoControl.reloadMods();
			flag_image_zoom = false;
		}
	}
	@FXML
	protected void averageFilter(ActionEvent event)
	{
		if(!flag_average_filter){
			MonadicImageTransform.transformList.add(MaskImageTransform.averageFilter);
			videoControl.reloadMods();
			flag_average_filter = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.averageFilter);
			videoControl.reloadMods();
			flag_average_filter = false;
		}
	}
	@FXML
	protected void medianFilter(ActionEvent event)
	{
		if(!flag_median_filter){
			MonadicImageTransform.transformList.add(MaskImageTransform.medianFilter);
			videoControl.reloadMods();
			flag_median_filter = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.medianFilter);
			videoControl.reloadMods();
			flag_median_filter = false;
		}
	}
	@FXML
	protected void sobel(ActionEvent event)
	{
		if(!flag_sobel){
			MonadicImageTransform.transformList.add(MaskImageTransform.sobelOperator);
			videoControl.reloadMods();
			flag_sobel = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.sobelOperator);
			videoControl.reloadMods();
			flag_sobel = false;
		}
	}
	@FXML
	protected void prewitt(ActionEvent event)
	{
		if(!flag_prewitt){
			MonadicImageTransform.transformList.add(MaskImageTransform.prewittOperator);
			videoControl.reloadMods();
			flag_prewitt = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.prewittOperator);
			videoControl.reloadMods();
			flag_prewitt = false;
		}
	}
	@FXML
	protected void compass(ActionEvent event)
	{
		if(!flag_compass){
			MonadicImageTransform.transformList.add(MaskImageTransform.prewittCompass);
			videoControl.reloadMods();
			flag_compass = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.prewittCompass);
			videoControl.reloadMods();
			flag_compass = false;
		}
	}
	@FXML
	protected void laplacianOne(ActionEvent event)
	{
		if(!flag_laplacian_one){
			MonadicImageTransform.transformList.add(MaskImageTransform.laplacianVersion1);
			videoControl.reloadMods();
			flag_laplacian_one = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.laplacianVersion1);
			videoControl.reloadMods();
			flag_laplacian_one = false;
		}
	}
	@FXML
	protected void laplacianTwo(ActionEvent event)
	{
		if(!flag_laplacian_two){
			MonadicImageTransform.transformList.add(MaskImageTransform.laplacianVersion2);
			videoControl.reloadMods();
			flag_laplacian_two = true;
		} else{
			MonadicImageTransform.transformList.remove(MaskImageTransform.laplacianVersion2);
			videoControl.reloadMods();
			flag_laplacian_two = false;
		}
	}
	@FXML
	protected void toggleCam(ActionEvent event)
	{
		videoControl.setColorCanvas(color_view);
		videoControl.setGreyCanvas(grey_view);
		try{
		videoControl.toggleCam();
		flag_cam_active = (flag_cam_active == false) ? true : false;
		} catch(Exception e){e.printStackTrace();}
	}
	@FXML
	protected void luminosity(ActionEvent event)
	{
		if(!flag_luminosity){
			MonadicImageTransform.transformList.add(MonadicImageTransform.luminosityGreyscale);
			videoControl.reloadMods();
			flag_luminosity = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.luminosityGreyscale);
			videoControl.reloadMods();
			flag_luminosity = false;
		}
	}
	@FXML
	protected void average(ActionEvent event)
	{
		if(!flag_average){
			MonadicImageTransform.transformList.add(MonadicImageTransform.averageGreyscale);
			videoControl.reloadMods();
			flag_average = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.averageGreyscale);
			videoControl.reloadMods();
			flag_average = false;
		}
	}
	@FXML
	protected void lightness(ActionEvent event)
	{
		if(!flag_lightness){
			MonadicImageTransform.transformList.add(MonadicImageTransform.lightnessGreyscale);
			videoControl.reloadMods();
			flag_lightness = true;
		} else{
			MonadicImageTransform.transformList.remove(MonadicImageTransform.lightnessGreyscale);
			videoControl.reloadMods();
			flag_lightness = false;
		}
	}
	@FXML
	protected void histogramEqualization(ActionEvent event)
	{
		if(!flag_histogram_equalization){
			MonadicImageTransform.transformList.add(HistogramImageTransform.histogramEqualization);
			videoControl.reloadMods();
			flag_histogram_equalization = true;
		} else{
			MonadicImageTransform.transformList.remove(HistogramImageTransform.histogramEqualization);
			videoControl.reloadMods();
			flag_histogram_equalization = false;
		}
	}
	@FXML
	protected void image1(ActionEvent event)
	{
		//Loading the first image
		//Sutting down the camera feed if it is active
		videoControl.setColorCanvas(color_view);
		videoControl.setGreyCanvas(grey_view);
		if(flag_cam_active)
		{
			this.toggleCam(event);
		}
		//Trying to load a buffered image from the URL.
		videoControl.loadImageOne(this.ta_image_url.getText());
		flag_first_image_loaded = true;
	}
	@FXML
	protected void image2(ActionEvent event)
	{
		//Loading the second image
		//Sutting down the camera feed if it is active
		videoControl.setColorCanvas(color_view);
		videoControl.setGreyCanvas(grey_view);
		if(flag_cam_active)
		{
			this.toggleCam(event);
		}
		//Trying to load a buffered image from the URL.
		videoControl.loadImageTwo(this.ta_image_url.getText());
		flag_second_image_loaded = true;
	}
	/*
	 * TODO:
	 * 1) Load images from text URL.
	 * 2) Read FxImage from URL.
	 * 3) Apply to color canvas.
	 * 4) Apply transforms including greyscale to image (once).
	 * 5) Paste image to greyscale canvas.
	 */
	//----------------------------------------------------------//
	public Controller()
	{
		videoControl = new VideoControl();
	}
	
}
