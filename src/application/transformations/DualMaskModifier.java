package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

public abstract class DualMaskModifier extends ImageTransform
{

	protected MaskModifier mask1;
	protected MaskModifier mask2;
	
	public DualMaskModifier()
	{
		setMasks();
	}
	
	@Override
	public boolean isDyadic() {
		return false;
	}

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	public Cell[][] applyMod(CellImage image) {
		//Cloning to apply operators on two dummy images.
		CellImage clone1 = image.cloneImage();
		CellImage clone2 = image.cloneImage();
		//Applying operators, very clever use of inheritance. I am awesome.
		clone1.applyTransform(mask1, null);
		clone2.applyTransform(mask2, null);
		//Summing the two images according to protocol.
		Cell[][] cells1 = clone1.getCellMatrix();
		Cell[][] cells2 = clone2.getCellMatrix();
		int width = cells1.length;
		int height = cells1[0].length;
		Cell[][] resultMatrix = new Cell[width][height];
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				//Pixel operation
				int nGrey = (int)
						Math.floor (
								Math.sqrt(
										Math.pow( cells1[x][y].getGrey(), 2 ) + Math.pow(  cells2[x][y].getGrey(), 2 ) 
										)
								);
				//--//
				//Verifying result
				nGrey = (nGrey < 0) ? 0 : (nGrey > 255) ? 255 : nGrey ;
				//Delivering result
				resultMatrix[x][y] = new Cell(nGrey);
			}
		}
		// Returning resulting matrix.
		return resultMatrix;
	}

	@Override
	public Cell[][] applyMod(CellImage image1, CellImage image2) {
		return null;
	}
	
	protected abstract void setMasks();

}
