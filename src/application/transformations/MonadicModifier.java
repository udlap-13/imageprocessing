package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

public abstract class MonadicModifier extends ImageTransform{

	@Override
	public boolean isDyadic() {
		return false;
	}

	@Override
	public abstract boolean isGreyscaleOp();

	@Override
	public Cell[][] applyMod(CellImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		Cell[][] resultMatrix = new Cell[width][height];
		Cell[][] originalMatrix = image.getCellMatrix();
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				Cell referenceCell = originalMatrix[x][y];
				resultMatrix[x][y] = pixelTransform(referenceCell);
			}
		}
		return resultMatrix;
	}

	@Override
	public Cell[][] applyMod(CellImage image1, CellImage image2) {
		return null;
	}
	
	protected abstract Cell pixelTransform(Cell originalCell);

}
