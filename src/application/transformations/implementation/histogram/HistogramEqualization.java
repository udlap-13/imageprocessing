package application.transformations.implementation.histogram;

import application.transformations.HistogramModifier;

/**
 * Created by Thagus on 08/03/2016.
 */
public class HistogramEqualization extends HistogramModifier {

    @Override
    protected int[] newHistogramValues(int[] histogram, double MN) {
        int[] newValues = new int[256];
        double[] probability = new double[256];

        double acum = 0;

        //Calcular la probabilidad de ocurrencia de un valor
        for(int i=0; i<256; i++){
            probability[i] = histogram[i]/MN;
        }

        for(int i=0; i<256; i++){
            acum += probability[i];
            newValues[i] = (int)(255*(acum));   //+0.5 for upward rounding
        }

        return newValues;
    }
}
