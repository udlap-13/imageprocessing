package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class Stretch extends MonadicModifier {

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		int oGrey = originalCell.getGrey();
		int greyTarget = 255;
		//Threshold we measure against
		int maxThreshold = 170;
		int minThreshold = 85;
		int nGrey = (oGrey > minThreshold && oGrey < maxThreshold) ? (oGrey - minThreshold) * (greyTarget/ (maxThreshold - minThreshold)) : 0;
		Cell nCell = new Cell(nGrey);
		return nCell;
	}

}
