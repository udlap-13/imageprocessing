package application.transformations.implementation.mask;

import application.transformations.MaskModifier;

public class AverageFilter extends MaskModifier{

	@Override
	protected void setMask() {
		this.mask = new double[3][3];
		//First row
		mask[0][0] = 0.11111111111;
		mask[1][0] = 0.11111111111;
		mask[2][0] = 0.11111111111;
		//Second row
		mask[0][1] = 0.11111111111;
		mask[1][1] = 0.11111111111;
		mask[2][1] = 0.11111111111;
		//Third row
		mask[0][2] = 0.11111111111;
		mask[1][2] = 0.11111111111;
		mask[2][2] = 0.11111111111;
	}
	
	public AverageFilter()
	{
		super();
	}

}
