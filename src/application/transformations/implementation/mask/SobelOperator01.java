package application.transformations.implementation.mask;

import application.transformations.MaskModifier;

public class SobelOperator01 extends MaskModifier{

	@Override
	protected void setMask() {
		this.mask = new double[3][3];
		//First row
		mask[0][0] = -1;
		mask[1][0] = -2;
		mask[2][0] = -1;
		//Second row
		mask[0][1] = 0;
		mask[1][1] = 0;
		mask[2][1] = 0;
		//Third row
		mask[0][2] = 1;
		mask[1][2] = 2;
		mask[2][2] = 1;
	}
	
	public SobelOperator01()
	{
		super();
	}
}
