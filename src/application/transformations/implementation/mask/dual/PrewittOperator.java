package application.transformations.implementation.mask.dual;

import application.control_center.MaskImageTransform;
import application.transformations.DualMaskModifier;

public class PrewittOperator extends DualMaskModifier
{

	@Override
	protected void setMasks() {
		this.mask1 = MaskImageTransform.prewittOperator01;
		this.mask2 = MaskImageTransform.prewittOperator02;
	}
	
	public PrewittOperator()
	{
		super();
	}
}
