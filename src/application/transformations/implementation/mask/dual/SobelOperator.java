package application.transformations.implementation.mask.dual;

import application.control_center.MaskImageTransform;
import application.transformations.DualMaskModifier;

public class SobelOperator extends DualMaskModifier
{

	@Override
	protected void setMasks() {
		this.mask1 = MaskImageTransform.sobelOperator01;
		this.mask2 = MaskImageTransform.sobelOperator02;
	}
	
	public SobelOperator()
	{
		super();
	}

}
