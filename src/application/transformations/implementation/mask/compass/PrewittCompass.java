package application.transformations.implementation.mask.compass;

import application.control_center.MaskImageTransform;
import application.transformations.CompassMaskModifier;

public class PrewittCompass extends CompassMaskModifier
{
	@Override
	protected void setMasks() {
		this.mask1 = MaskImageTransform.prewittOperator01;
		this.mask2 = MaskImageTransform.prewittOperator02;
		
		this.mask3 = MaskImageTransform.prewittOperator03;
		this.mask4 = MaskImageTransform.prewittOperator04;
		
		this.mask5 = MaskImageTransform.prewittOperatorD01;
		this.mask6 = MaskImageTransform.prewittOperatorD02;
		
		this.mask7 = MaskImageTransform.prewittOperatorD03;
		this.mask8 = MaskImageTransform.prewittOperatorD04;
	}
	
	public PrewittCompass()
	{
		super();
	}

}
