package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class LightnessGreyscale extends MonadicModifier{

	@Override
	public boolean isGreyscaleOp() {
		return true;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		int oRed = originalCell.getRed();
		int oGreen = originalCell.getGreen();
		int oBlue = originalCell.getBlue();
		int[] colors = new int[3];
		colors[0] = oRed;
		colors[1] = oGreen;
		colors[2] = oBlue;
		int maxLight = 0;
		int minLight = 255;
		for(int i = 0; i < 3; i++)
		{
			if(colors[i] > maxLight)
				maxLight = colors[i];
			if(colors[i] < minLight)
				minLight = colors[i];
		}
		int grey = (int)(maxLight + minLight)/2;
		Cell genCell = new Cell(oRed, oGreen, oBlue, grey);
		return genCell;
	}

}
