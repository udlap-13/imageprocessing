package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class inverseOperator extends MonadicModifier
{
	/*
	 * Test method to see if the framework performs as expected.
	 */

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		int nGrey = 255 - originalCell.getGrey();
		Cell nCell = new Cell(nGrey);
		return nCell;
	}
	
}
