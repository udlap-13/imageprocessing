package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class GreyLevelReduction extends MonadicModifier {
	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		int oGrey = originalCell.getGrey();
		//Number of greys
		int targetGreys = 15;
		//----------------------//
		int greyUnit = 255/ targetGreys;
		if(2 * greyUnit <= 255)
		{
			for(int i = greyUnit; i <= 255; i += greyUnit)
			{
				int lowLimit = i;
				int highLimit = i + greyUnit;
				if(oGrey >= lowLimit && (oGrey <= highLimit || i + greyUnit > 255))
				{
					Cell rCell = new Cell(i);
					return rCell;
				}
			}
			return new Cell(0);
		}
		else
		{
			return new Cell(0);
		}
	}
}
