package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class InvGreyscaleThreshold extends MonadicModifier {
	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		//Getting greyscale level from original cell
		int oGrey = originalCell.getGrey();
		//Threshold we measure against
		int maxThreshold = 170;
		int minThreshold = 85;
		int nGrey = (oGrey > minThreshold && oGrey < maxThreshold) ? 255 - oGrey : 0;
		Cell nCell = new Cell(nGrey);
		return nCell;
	}
}
