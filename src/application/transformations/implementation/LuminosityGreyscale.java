package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class LuminosityGreyscale extends MonadicModifier
{

	@Override
	public boolean isGreyscaleOp() {
		return true;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		int oRed = originalCell.getRed();
		int oGreen = originalCell.getGreen();
		int oBlue = originalCell.getBlue();
		int grey = (int)(oRed * 0.21 + oGreen * 0.72 + oBlue * 0.07);
		Cell genCell = new Cell(oRed, oGreen, oBlue, grey);
		return genCell;
	}
	
}
