package application.transformations.implementation.unique;

import application.cell_image.Cell;
import application.cell_image.CellImage;
import application.transformations.MonadicModifier;

public class ImageZoom extends MonadicModifier
{
	
	private double zoomV = 2;

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	public Cell[][] applyMod(CellImage image) {
		//Old image dimensions
		int width = image.getWidth();
		int height = image.getHeight();
		//New image dimensions
		int nWidth = (int) Math.ceil(width * zoomV);
		int nHeight = (int) Math.ceil(height * zoomV);
		//Measurements for pixel allocation, using ceil to not have any black pixels in scaled image.
		//No kill like overkill basically.
		int widthTerritory = (int)Math.ceil((nWidth - width) / width + 1);
		
		int heightTerritory = (int)Math.ceil((nHeight - height) / height + 1);
		
		//The matrixes we will work with.
		Cell[][] resultMatrix = new Cell[width][height];
		Cell[][] originalMatrix = image.getCellMatrix();
		//Sampling the original matrix and allocating nearest neighbor pixel to new territories.
		int x = 0;
		while(x * widthTerritory < width)
		{
			int y = 0;
			while(y * heightTerritory < height)
			{
				//New coordinates with correct position adjustment for territory allocation.
				int tX = x * widthTerritory;
				int tY = y * heightTerritory;
				//Verifying coordinates
				tX = (tX >= width) ? width - 1 : (tX < 0) ? 0 : tX ;
				tY = (tY >= height) ? height - 1 : (tY < 0) ? 0 : tY ;
				//Populating result matrix with  adjusted basic image pixels.
				resultMatrix[tX][tY] = pixelTransform(originalMatrix[x][y]);
				 // So far the image has been scaled but without interpolation, now we will interpolate.
				int iX = tX;		// X loop
				while(iX < (x+1) * widthTerritory && x + 1 < width)
				{
					int iY = tY;	// Y loop
					while(iY < (y + 1) * heightTerritory && y + 1 < height)
					{
						//Guarding for out of bounds coordinates.
						if(iX < width && iY < height)		//We might have to check if this pixel is not a pivot, time will tell.
						{
							//Then we proceed:
							resultMatrix[iX][iY] = pixelTransform(originalMatrix[x][y]);
						}
						//Counter ++
						iY++;
					}
					//Counter ++
					iX++;
				}
				y++;
			}
			x++;
		}
		return resultMatrix;
	}
	
	@Override
	protected Cell pixelTransform(Cell originalCell) {
		Cell nCell = new Cell(originalCell.getGrey());
		return nCell;
	}

}
