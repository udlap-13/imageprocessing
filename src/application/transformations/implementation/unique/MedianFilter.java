package application.transformations.implementation.unique;

import java.util.Arrays;

import application.cell_image.Cell;
import application.transformations.MaskModifier;

public class MedianFilter extends MaskModifier{
	
	@Override
	protected Cell maskOp(Cell[][] maskedArea) {
		//Getting the number of elements sampled
		int dataSize = maskedArea.length * maskedArea[0].length;
		//This contains all of the values of the mask
		int pixelValues[] = new int[dataSize];
		int pCounter = 0;
		//Extracting data from masked area.
		for(int x = 0; x < maskedArea.length; x++)
		{
			for(int y = 0; y < maskedArea[0].length; y++)
			{
				pixelValues[pCounter] = maskedArea[x][y].getGrey();
				pCounter++;
			}
		}
		//Sorting pixels.
		Arrays.sort(pixelValues);
		//Getting the median value.
		int medianIndex = (int) Math.floor(pixelValues.length / 2);
		int medianGrey = pixelValues[medianIndex];
		//Building and returning the new cell
		Cell resultCell = new Cell(medianGrey);
		return resultCell;
	}
	
	@Override
	protected void setMask() {
		this.mask = new double[3][3];
		//First row
		mask[0][0] = 0;
		mask[1][0] = 0;
		mask[2][0] = 0;
		//Second row
		mask[0][1] = 0;
		mask[1][1] = 0;
		mask[2][1] = 0;
		//Third row
		mask[0][2] = 0;
		mask[1][2] = 0;
		mask[2][2] = 0;
	}
	
	public MedianFilter()
	{
		super();
	}

}
