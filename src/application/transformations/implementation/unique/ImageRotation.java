package application.transformations.implementation.unique;

import application.cell_image.Cell;
import application.cell_image.CellImage;
import application.transformations.MonadicModifier;

public class ImageRotation extends MonadicModifier{

	//New stuff+
	private double cosV = 0.7071;
	private double sinV = 0.7071;
	
	//-------------//
	@Override
	public boolean isGreyscaleOp() {
		return false;
	}
	
	@Override
	public Cell[][] applyMod(CellImage image) {
		//Rotation stuff
		int width = image.getWidth();
		int height = image.getHeight();
		Cell[][] resultMatrix = new Cell[width][height];
		Cell[][] originalMatrix = image.getCellMatrix();
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				//Getting new coordinates
				int nX = (int)( (x - width/2) * cosV - (y - height/2) * sinV  + width/2);
				nX = (nX >= width) ? width - 1 : (nX < 0) ? 0 : nX ;
				
				int nY = (int)( (x - width/2) * sinV + (y - height/2) * cosV + height/2);
				nY = (nY >= height) ? height - 1 : (nY < 0) ? 0 : nY ;
				
				//Filling result matrix with results
				Cell referenceCell = originalMatrix[x][y];
				resultMatrix[nX][nY] = pixelTransform(referenceCell);
				
				//Filling result matrix with black values just in case this pixel is out of the rotation.
				if(resultMatrix[x][y] == null)
				{
					resultMatrix[x][y] = new Cell(0);
				}
			}
		}
		return resultMatrix;
	}
	
	public void setSinTheta( double val )
	{
		sinV = val;
	}
	public void setCosTheta( double val )
	{
		cosV = val;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		Cell nCell = new Cell(originalCell.getGrey());
		return nCell;
	}
	
	

}
