package application.transformations.implementation.dyadic;

import application.cell_image.Cell;
import application.transformations.DyadicModifier;

/**
 * Created by Thagus on 08/03/2016.
 */
public class ImageAddition extends DyadicModifier {
    @Override
    protected Cell pixelTransform(Cell cell1, Cell cell2) {
        int red = (int)((cell1.getRed() + cell2.getRed())/2 + 0.5);         //+0.5 values rounded upward
        int green = (int)((cell1.getGreen() + cell2.getGreen())/2 + 0.5);
        int blue = (int)((cell1.getBlue() + cell2.getBlue())/2 + 0.5);
        int gray = (int)((cell1.getGrey() + cell2.getGrey())/2 + 0.5);
        Cell genCell = new Cell(red, green, blue, gray);
        return genCell;
    }

    protected Cell[][] scaling(Cell[][] matrix){
        return matrix;
    }
}
