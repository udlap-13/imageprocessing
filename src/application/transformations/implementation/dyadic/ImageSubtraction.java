package application.transformations.implementation.dyadic;

import application.cell_image.Cell;
import application.transformations.DyadicModifier;

/**
 * Created by Thagus on 08/03/2016.
 */
public class ImageSubtraction extends DyadicModifier {
    @Override
    protected Cell pixelTransform(Cell cell1, Cell cell2) {
        int red = (cell1.getRed() - cell2.getRed());
        int green = (cell1.getGreen() - cell2.getGreen());
        int blue = (cell1.getBlue() - cell2.getBlue());
        int gray = (cell1.getGrey() - cell2.getGrey());
        //System.out.println(gray);
        Cell genCell = new Cell(red, green, blue, gray);
        return genCell;
    }

    protected Cell[][] scaling(Cell[][] matrix){
        int cmax = findMax(matrix);
        int cmin = findMin(matrix);

        int width = matrix.length;
        int height = matrix[0].length;

        int gray;
        Cell[][] result = new Cell[width][height];

        for(int i=0; i<width; i++){		//ancho
            for(int j=0; j<height; j++){	//alto
                gray = (int)((matrix[i][j].getGrey() - cmin)*(255/(double)(Math.abs(cmin)+Math.abs(cmax))));
                result[i][j] = new Cell(gray);
            }
        }

        return result;
    }

    private int findMin(Cell[][] matrix){
        int width = matrix.length;
        int height = matrix[0].length;

        int min = 255;

        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){
                if(matrix[i][j].getGrey() < min){
                    min = matrix[i][j].getGrey();
                }
            }
        }
        return min;
    }

    private int findMax(Cell[][] matrix){
        int width = matrix.length;
        int height = matrix[0].length;

        int max = 0;

        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){
                if(matrix[i][j].getGrey() > max){
                    max = matrix[i][j].getGrey();
                }
            }
        }
        return max;
    }
}
