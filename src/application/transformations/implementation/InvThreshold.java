package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class InvThreshold extends MonadicModifier {
	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		//Getting greyscale level from original cell
		int oGrey = originalCell.getGrey();
		//Threshold we measure against
		int threshold = 127;
		int nGrey = (oGrey < threshold) ? 255 : 0;
		Cell nCell = new Cell(nGrey);
		return nCell;
	}

}
