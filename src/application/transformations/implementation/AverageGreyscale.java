package application.transformations.implementation;

import application.cell_image.Cell;
import application.transformations.MonadicModifier;

public class AverageGreyscale extends MonadicModifier
{

	@Override
	public boolean isGreyscaleOp() {
		return true;
	}

	@Override
	protected Cell pixelTransform(Cell originalCell) {
		int oRed = originalCell.getRed();
		int oGreen = originalCell.getGreen();
		int oBlue = originalCell.getBlue();
		int grey = (int)(oRed + oGreen + oBlue) / 3;
		Cell genCell = new Cell(oRed, oGreen, oBlue, grey);
		return genCell;
	}

}
