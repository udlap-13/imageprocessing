package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

public abstract class MaskModifier extends ImageTransform{
	
	protected double[][] mask;

	@Override
	public boolean isDyadic() {
		return false;
	}

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	public Cell[][] applyMod(CellImage image) {
		//Offset to move mask before applying to prevent using edges
		int maskWidth = (int)Math.floor(mask.length / 2);
		//Remember that masks are always odd numbered.
		//This is the entire image
		Cell[][] map = image.getCellMatrix();
		//This is the convoluted image that we will work upon
		Cell[][] convMap = new Cell[ map.length ][ map[0].length ];
		//Ops to ignore edges
		if(map.length > maskWidth && map[0].length > maskWidth)
		{
			for(int x = maskWidth; x < map.length - maskWidth; x++)
			{
				for(int y = maskWidth; y < map[0].length - maskWidth; y++)
				{
					//We will toss all relevant cells in this array for operation
					Cell[][] scoopedMatrix = new Cell[2 * maskWidth + 1][2 * maskWidth + 1];
					//Scooping mask, mask is always odd numbered so x is the center horizontally
					//scoopedMatrix data
					int x3 = 0;
					int y3 = 0;
					for(int x2 = x - maskWidth; x2 <= x + maskWidth; x2++)
					{
						//Y is always in the center of the vertical axis.
						for(int y2 = y - maskWidth; y2 <= y + maskWidth; y2++)
						{
							scoopedMatrix[x3][y3] = map[x2][y2];
							//Adding to counter
							y3++;
						}
						//Adding to counter
						x3++;
						y3 = 0;
					}
					//Applying the mask operation and get the cell for this area's center cell
					Cell newCell = maskOp(scoopedMatrix);
					convMap[x][y] = newCell;
				}
			}
			//Filling the edges of the convoluted image:
			//Leftmost edge
			for(int x = 0; x < maskWidth; x++)
			{
				for(int y = 0; y < map[0].length; y++)
				{
					convMap[x][y] = new Cell(0);
				}
			}
			//Rightmost edge
			for(int x = map.length - maskWidth; x < map.length; x++)
			{
				for(int y = 0; y < map[0].length; y++)
				{
					convMap[x][y] = new Cell(0);
				}
			}
			//Top edge
			for(int x = maskWidth; x < map.length - maskWidth; x++)
			{
				for(int y = 0; y < maskWidth; y++)
				{
					convMap[x][y] = new Cell(0);
				}
			}
			//Bottom edge
			for(int x = maskWidth; x < map.length - maskWidth; x++)
			{
				for(int y = map[0].length - maskWidth; y < map[0].length; y++)
				{
					convMap[x][y] = new Cell(0);
				}
			}
			//Now the convoluted image is complete.
			return convMap;
		}
		else
		{
			//If the mask was too large to apply simply return the original cell map.
			return map;
		}
	}

	@Override
	public Cell[][] applyMod(CellImage image1, CellImage image2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected Cell maskOp( Cell[][] maskedArea )
	{
		double nGrey = 0;
		for(int x = 0; x < mask.length; x++)
		{
			for(int y = 0; y < mask[0].length; y++)
			{
				double modGrey = maskedArea[x][y].getGrey() * mask[x][y];
				nGrey += modGrey;
			}
		}
		//Capping nGrey
		if(nGrey > 255) nGrey = 255;
		if(nGrey < 0) nGrey = 0;
		Cell nCell = new Cell((int)Math.floor(nGrey));
		return nCell;
	}

	protected abstract void setMask();
	
	public MaskModifier()
	{
		setMask();
	}
}
