package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

public abstract class CompassMaskModifier extends ImageTransform
{
	//More awesome clever polymorphism, these are actually dyadic, but the system
	//does not care and they could very well be monadic without any problem.
	protected MaskModifier mask1;
	protected MaskModifier mask2;
	protected MaskModifier mask3;
	protected MaskModifier mask4;
	
	protected MaskModifier mask5;
	protected MaskModifier mask6;
	protected MaskModifier mask7;
	protected MaskModifier mask8;
	
	private MaskModifier[] maskArray = new MaskModifier[8];
	
	public CompassMaskModifier()
	{
		setMasks();
		maskArray[0] = mask1;
		maskArray[1] = mask2;
		maskArray[2] = mask3;
		maskArray[3] = mask4;
		
		maskArray[4] = mask5;
		maskArray[5] = mask6;
		maskArray[6] = mask7;
		maskArray[7] = mask8;
	}
	
	@Override
	public boolean isDyadic() {
		return false;
	}

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	public Cell[][] applyMod(CellImage image) {
		//Cloning the image all the times we will need to use, since we need 8 directions.
		CellImage[] clonedCellMaps= new CellImage[8];
		for(int c = 0; c < 8; c++)
		{
			CellImage clone = image.cloneImage();
			clonedCellMaps[c] = clone;
		}
		//Preparing to cycle through the original image.
		//Now we cycle through each angle and in each carry out the operation.
		Cell[][] refOnlyCellMap = clonedCellMaps[0].getCellMatrix();		//It only serves the purpose of getting dimensions.
		int width = refOnlyCellMap.length;
		int height = refOnlyCellMap[0].length;
		refOnlyCellMap = null;				//We don't need it anymore so we nullify it.
		Cell[][] resultMatrix = new Cell[width][height];		//We shall store the results here. According to a max value.
		//Now we proceed to carry out the work. We apply all transforms to the clones
		//We also prepare a CellMatrix array where we will store the cell matrix without the image overhead, accelerating our runtime.
		Cell[][][] cellMatrixes = new Cell[8][width][height];
		for(int c = 0; c < 8; c++)
		{
			CellImage chosenImage = clonedCellMaps[c];
			chosenImage.applyTransform(maskArray[c], null);	//One transform for each cloned image
			cellMatrixes[c] = chosenImage.getCellMatrix();
		}
		//With the transforms now applied, we then check which one had the best result.
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				int nGrey = 0;
				for(int i = 0; i < 8; i++)
				{
					int cellGrey = cellMatrixes[i][x][y].getGrey();
					nGrey = (cellGrey > nGrey) ? cellGrey : nGrey ; 
				}
				//Verifying result
				nGrey = (nGrey < 0) ? 0 : (nGrey > 255) ? 255 : nGrey ;
				//Populating our matrix with our result
				resultMatrix[x][y] = new Cell(nGrey);
			}
		}
		
		// Returning resulting matrix.
		return resultMatrix;
	}

	@Override
	public Cell[][] applyMod(CellImage image1, CellImage image2) {
		return null;
	}
	
	protected abstract void setMasks();
}
