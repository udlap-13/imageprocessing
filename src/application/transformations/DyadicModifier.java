package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

public abstract class DyadicModifier extends ImageTransform{

	@Override
	public boolean isDyadic() {
		return true;
	}

	@Override
	public boolean isGreyscaleOp() {
		return false;
	}

	@Override
	public Cell[][] applyMod(CellImage image) {
		return null;
	}

	@Override
	public Cell[][] applyMod(CellImage image1, CellImage image2) {
		int width1 = image1.getWidth();
		int height1 = image1.getHeight();
		int width2 = image2.getWidth();
		int height2 = image2.getHeight();

		int width = (width1 < width2) ? width1 : width2;
		int height = (height1 < height2) ? height1 : height2;

		Cell[][] resultMatrix = new Cell[width][height];

		Cell[][] originalMatrix1 = image1.getCellMatrix();
		Cell[][] originalMatrix2 = image2.getCellMatrix();
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				Cell referenceCell1 = originalMatrix1[x][y];
				Cell referenceCell2 = originalMatrix2[x][y];

				resultMatrix[x][y] = pixelTransform(referenceCell1, referenceCell2);
			}
		}
		return scaling(resultMatrix);

	}

	protected abstract Cell[][] scaling(Cell[][] matrix);
	
	protected abstract Cell pixelTransform(Cell cell1, Cell cell2);

}
