package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

public abstract class ImageTransform 
{
	public abstract boolean isDyadic();
	
	public abstract boolean isGreyscaleOp();
	
	public abstract Cell[][] applyMod(CellImage image);
	
	public abstract Cell[][] applyMod(CellImage image1, CellImage image2);
}
