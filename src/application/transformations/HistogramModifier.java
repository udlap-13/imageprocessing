package application.transformations;

import application.cell_image.Cell;
import application.cell_image.CellImage;

/**
 * Created by Thagus on 08/03/2016.
 */
public abstract class HistogramModifier extends ImageTransform {
    @Override
    public boolean isDyadic() {
        return false;
    }

    @Override
    public boolean isGreyscaleOp() {
        return false;
    }

    @Override
    public Cell[][] applyMod(CellImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        double MN = width*height;

        Cell[][] resultMatrix = new Cell[width][height];
        Cell[][] originalMatrix = image.getCellMatrix();

        int[] histogram = new int[256];
        int[] newValues;
        int value;

        //Popular histograma
        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){
                value = originalMatrix[i][j].getGrey();
                histogram[value]++;
            }
        }

        //Calcular nuevos valores para pixeles dependiendo del histograma y el tamaño de la imagen
        newValues = newHistogramValues(histogram, MN);

        //Camciar imagen para que tenga los nuevos valores
        for(int i=0; i<width; i++){
            for(int j=0; j<height; j++){
                value = originalMatrix[i][j].getGrey();
                resultMatrix[i][j] = new Cell(newValues[value]);
            }
        }

        return resultMatrix;
    }

    protected abstract int[] newHistogramValues(int[] histogram, double MN);

    @Override
    public Cell[][] applyMod(CellImage image1, CellImage image2) {
        return null;
    }
}
